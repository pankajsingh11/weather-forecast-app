describe("Weather app", function () {
  beforeEach(function () {
    cy.visit("http://localhost:3000");
  });

  it("search for sydney city", function () {
    cy.wait(2000);
    cy.get("input").type("s");
    cy.wait(500);
    cy.get("input").type("y");
    cy.wait(500);
    cy.get("input").type("d");
    cy.wait(500);
    cy.get("input").type("n");
    cy.wait(500);
    cy.get("input").type("e");
    cy.wait(500);
    cy.get("input").type("y");
    cy.wait(1000);
    cy.get("[data-test]:first").click();
    cy.contains("Sydney, Australia");
  });
  it("search for kanpur city", function () {
    cy.wait(2000);
    cy.get("input").type("k");
    cy.wait(500);
    cy.get("input").type("a");
    cy.wait(500);
    cy.get("input").type("n");
    cy.wait(500);
    cy.get("input").type("p");
    cy.wait(500);
    cy.get("input").type("u");
    cy.wait(500);
    cy.get("input").type("r");
    cy.wait(1000);
    cy.get("[data-test]:first").click();
    cy.contains("Kanpur, India");
  });
});
