import { getCities, getWeatherForecast } from "./weather";
import axios from "axios";
import { forecastData } from "../mock/mockData";
jest.mock("axios");
describe("weather service tests", () => {
  test("getCity returns cities object", async () => {
    axios.get.mockImplementationOnce(() =>
      Promise.resolve({ data: [{ name: "Sydney, New South Wales" }] })
    );
    const result = await getCities("sydney");
    expect(result).toStrictEqual([{ name: "Sydney, New South Wales" }]);
  });
  test("getCity returns error object when call fails", async () => {
    axios.get.mockImplementationOnce(() =>
      Promise.reject({ error: "fake error" })
    );

    try {
      await getCities("sydney");
    } catch (e) {
      expect(e).toStrictEqual(Error({ error: "fake error" }));
    }
  });

  test("getWeatherForecast returns forecast data", async () => {
    axios.get.mockImplementationOnce(() =>
      Promise.resolve({
        data: forecastData,
      })
    );
    const result = await getWeatherForecast("sydney");
    expect(result).toStrictEqual(forecastData);
  });
  test("getWeatherForecast returns error object when call fails", async () => {
    axios.get.mockImplementationOnce(() =>
      Promise.reject({ error: "fake error" })
    );

    try {
      await getWeatherForecast("sydney");
    } catch (e) {
      expect(e).toStrictEqual(Error({ error: "fake error" }));
    }
  });
});
