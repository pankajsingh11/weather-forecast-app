import axios from "axios";
require("dotenv").config();
const API_KEY = process.env.REACT_APP_API_KEY || "";
const baseURL = `https://api.weatherapi.com/v1/`;
const forecastRoute = `${baseURL}forecast.json?key=${API_KEY}&q=`;
const searchRoute = `${baseURL}search.json?key=${API_KEY}&q=`;
export const getWeatherForecast = async ({
  cityName,
  lat = null,
  long = null,
}) => {
  try {
    let response = {};
    if (lat && long) {
      response = await axios.get(
        `${forecastRoute}${lat},${long}&days=5&aqi=no`
      );
    } else {
      response = await axios.get(`${forecastRoute}${cityName}&days=5&aqi=no`);
    }
    return response.data;
  } catch (error) {
    console.log("Error ocurred while fetching weather data", error);
    throw Error(error);
  }
};
export const getCities = async (searchTerm) => {
  try {
    const response = await axios.get(`${searchRoute}${searchTerm}`);
    return response.data;
  } catch (error) {
    console.log("Error ocurred while fetching weather data", error);
    throw Error(error);
  }
};
