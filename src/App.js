import AutoCompleteSearch from "./components/AutoCompleteSerach";
import WeatherCard from "./components/WeatherCard";
import { useEffect, useState } from "react";
import { getWeatherForecast } from "./services/weather";
import ForecastCard from "./components/ForecastCard";
import LoadingCard from "./components/LoadingCard";
import { ErrorBoundary } from "react-error-boundary";
import { toast, ToastContainer } from "react-toastify";
import ErrorFallback from "./components/ErrorFallback";
import "react-toastify/dist/ReactToastify.css";
function App() {
  const [weatherData, setWeatherData] = useState(null);
  const [loading, setLoading] = useState(false);
  const options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0,
  };
  const errors = (err) => {
    console.warn(`ERROR(${err.code}): ${err.message}`);
  };
  const success = async ({ coords }) => {
    try {
      setLoading(true);
      const result = await getWeatherForecast({
        lat: coords.latitude,
        long: coords.longitude,
      });
      setWeatherData(result);
      setLoading(false);
    } catch (error) {
      console.log(error);
      toast(error.message);
    }
  };
  useEffect(() => {
    if (navigator.geolocation) {
      navigator.permissions
        .query({ name: "geolocation" })
        .then(function (result) {
          if (result.state === "granted") {
            //If granted then you can directly call your function here
            navigator.geolocation.getCurrentPosition(success);
          } else if (result.state === "prompt") {
            navigator.geolocation.getCurrentPosition(success, errors, options);
          } else if (result.state === "denied") {
            //If denied then you have to show instructions to enable location
          }
          result.onchange = function () {
            console.log(result.state);
          };
        });
    } else {
      console.log("Geo location not available in your browser");
      toast("Geo location not available in your browser");
    }
  }, []);
  const handleChange = async (cityName) => {
    try {
      setLoading(true);
      const result = await getWeatherForecast({ cityName });
      setWeatherData(result);
      setLoading(false);
    } catch (error) {
      console.log(error);
      toast(error.message);
    }
  };
  const getWeatherCardWithLoading = () => {
    if (loading) {
      return <LoadingCard />;
    } else if (weatherData) {
      return (
        <>
          <WeatherCard weatherData={weatherData} />
          <ForecastCard forecastData={weatherData.forecast.forecastday} />
        </>
      );
    } else {
      return null;
    }
  };
  return (
    <div className="app">
      <main>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <ErrorBoundary FallbackComponent={ErrorFallback}>
          <AutoCompleteSearch handleCitySelect={handleChange} />
          {getWeatherCardWithLoading()}
        </ErrorBoundary>
      </main>
    </div>
  );
}

export default App;
