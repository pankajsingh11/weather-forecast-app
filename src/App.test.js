import { render, screen, fireEvent } from "@testing-library/react";
import { renderHook, act } from "@testing-library/react-hooks";
import App from "./App";

describe("App integration tests", () => {
  test("renders searchbox", () => {
    render(<App />);
    const searchBox = screen.getByPlaceholderText("Please enter city name");
    expect(searchBox).toBeInTheDocument();
  });
  test("allows to enter search string", () => {
    const component = render(<App />);
    const searchBox = component.getByPlaceholderText("Please enter city name");
    expect(searchBox.value).toBe("");
    fireEvent.change(searchBox, {
      target: { value: "sydney" },
    });
    expect(searchBox.value).toBe("sydney");
  });
});
