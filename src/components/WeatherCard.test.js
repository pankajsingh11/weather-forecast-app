import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import WeatherCard from "./WeatherCard";
import { forecastData } from "../mock/mockData";
describe("WeatherCard", () => {
  test("renders without crash", () => {
    const component = render(<WeatherCard weatherData={forecastData} />);
    const WeatherCardComponent = component.getByTestId("weather");
    expect(WeatherCardComponent).toBeInTheDocument();
  });
  test("renders location and weatherCard", () => {
    const component = render(<WeatherCard weatherData={forecastData} />);
    const locationComponent = component.getByTestId("location");
    expect(locationComponent).toBeInTheDocument();
    const weatherCardComponent = component.getByTestId("weatherCard");
    expect(weatherCardComponent).toBeInTheDocument();
    const allWeatherCard = component.getAllByTestId("weatherCard");
    expect(allWeatherCard).toHaveLength(1);
  });
});
