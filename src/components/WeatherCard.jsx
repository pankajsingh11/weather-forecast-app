import React from "react";
import Location from "./Location";
import Weather from "./Weather";

const WeatherCard = ({ weatherData }) => {
  return (
    <div data-testid="weather" className="weather-container">
      <Location
        locationDisplay={`${weatherData.location.name}, ${weatherData.location.country}`}
      />
      <Weather
        temp={weatherData.current.temp_c}
        condition={weatherData.current.condition}
        imgLocation={weatherData.current.condition.text}
        date={weatherData.location.localtime}
      />
    </div>
  );
};
export default WeatherCard;
