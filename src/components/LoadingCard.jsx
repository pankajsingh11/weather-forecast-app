import React from "react";

const LoadingCard = () => {
  return (
    <div data-testid="loadingCard" className="weather-container loading">
      <div className="weather-container">
        <div className="location-container animate">
          <div className="location"></div>
        </div>
        <div className="weather-card animate">
          <div className="date animate"></div>
          <div className="temp animate"></div>
          <div className="img animate"></div>
          <div className="weather animate"></div>
        </div>
      </div>
    </div>
  );
};
export default LoadingCard;
