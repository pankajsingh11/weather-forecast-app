import React, { useState } from "react";
import { getCities } from "../services/weather";
import { ReactSearchAutocomplete } from "react-search-autocomplete";
import { toast } from "react-toastify";
const AutoCompleteSearch = ({ handleCitySelect }) => {
  const [items, setItems] = useState([]);

  const handleOnSearch = async (value) => {
    try {
      const searchItems = await getCities(value);
      setItems(searchItems);
    } catch (error) {
      console.log(error);
      toast(error.message);
    }
  };
  const handleOnSelect = (item) => {
    handleCitySelect(item.url);
  };
  return (
    <ReactSearchAutocomplete
      items={items}
      onSearch={handleOnSearch}
      onSelect={handleOnSelect}
      placeholder="Please enter city name"
      autoFocus
    />
  );
};
export default AutoCompleteSearch;
