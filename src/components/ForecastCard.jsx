import React from "react";
import Weather from "./Weather";

const ForecastCard = ({ forecastData }) => {
  // eslint-disable-next-line no-unused-vars
  const [currentDayData, ...remaingDaysData] = forecastData;
  return (
    <div data-testid="forecast" className="forecast weather-container">
      {remaingDaysData.map((weather) => (
        <Weather
          temp={weather.day.avgtemp_c}
          condition={weather.day.condition}
          date={weather.date}
          key={weather.date}
        />
      ))}
    </div>
  );
};
export default ForecastCard;
