import { toast } from "react-toastify";
const ErrorFallback = ({ error }) => {
  console.log("error ocurred calling toast");
  return toast(error.message);
};
export default ErrorFallback;
