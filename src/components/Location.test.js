import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import Location from "./Location";

describe("AutoCompleteSearch", () => {
  test("renders without crash", () => {
    const component = render(
      <Location locationDisplay="Sydney, New South Wales, Australia" />
    );
    const locationContainer = component.getByTestId("location");
    expect(locationContainer).toBeInTheDocument();
  });
  test("displays correct location", () => {
    const component = render(
      <Location locationDisplay="Sydney, New South Wales, Australia" />
    );
    const locationDisplay = component.getByText(
      "Sydney, New South Wales, Australia"
    );
    expect(locationDisplay).toBeInTheDocument();
  });
});
