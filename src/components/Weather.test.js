import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import Weather from "./Weather";

describe("Weather", () => {
  test("renders without crash", () => {
    const component = render(
      <Weather
        temp="12"
        condition={{ icon: "//blaah", text: "sunny" }}
        date="21-08-2021"
      />
    );
    const weatherCard = component.getByTestId("weatherCard");
    expect(weatherCard).toBeInTheDocument();
  });

  test("displays correct properties", () => {
    const component = render(
      <Weather
        temp="12"
        condition={{ icon: "//blaah", text: "sunny" }}
        date="2021-08-21"
      />
    );

    const dateElement = component.getByText("Sat Aug 21 2021");
    expect(dateElement).toBeInTheDocument();
    const temperatureElement = component.getByText("12 °C");
    expect(temperatureElement).toBeInTheDocument();
    const imgElement = component.getByAltText("sunny");
    expect(imgElement).toBeInTheDocument();
    expect(imgElement.src).toBe("http://blaah/");
    expect(imgElement.title).toBe("sunny");
    const weatherElement = component.getByText("sunny");
    expect(weatherElement).toBeInTheDocument();
  });
});
