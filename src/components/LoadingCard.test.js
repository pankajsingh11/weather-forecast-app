import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import LoadingCard from "./LoadingCard";

describe("LoadingCard", () => {
  test("renders without crash", () => {
    const component = render(<LoadingCard />);
    const loadingComponent = component.getByTestId("loadingCard");
    expect(loadingComponent).toBeInTheDocument();
  });
});
