import React from "react";

const Location = ({ locationDisplay }) => {
  return (
    <div data-testid="location" className="location-container">
      <div className="location">{locationDisplay}</div>
    </div>
  );
};
export default Location;
