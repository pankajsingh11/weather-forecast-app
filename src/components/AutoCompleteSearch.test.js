import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent, cleanup, act } from "@testing-library/react";
import AutoCompleteSearch from "./AutoCompleteSerach";
//import { getCity } from "../services/weather";
//import { items } from "../mock/mockData";
beforeEach(() => {
  localStorage.clear();
});
afterEach(() => {
  cleanup();
  jest.clearAllMocks();
});
describe("AutoCompleteSearch", () => {
  beforeEach(() => jest.useFakeTimers());

  afterEach(() => {
    jest.clearAllMocks();
  });
  test("renders without crash", () => {
    const component = render(<AutoCompleteSearch />);
    const inputSearch = component.getByPlaceholderText(
      "Please enter city name"
    );
    expect(inputSearch).toBeInTheDocument();
  });
  test("enter search term", () => {
    const handleCitySelect = jest.fn();
    const component = render(
      <AutoCompleteSearch handleCitySelect={handleCitySelect} />
    );
    const inputSearch = component.getByPlaceholderText(
      "Please enter city name"
    );
    fireEvent.change(inputSearch, {
      target: { value: "sydney" },
    });

    expect(inputSearch.value).toBe("sydney");
  });
});
