import React from "react";

const Weather = ({ temp, condition, date }) => {
  const displayDate = new Date(date);
  return (
    <div data-testid="weatherCard" className="weather-card">
      <div className="date">{displayDate.toDateString()}</div>
      <div className="temp">{temp} &deg;C</div>
      <img src={condition.icon} alt={condition.text} title={condition.text} />
      <div className="weather">{condition.text}</div>
    </div>
  );
};
export default Weather;
