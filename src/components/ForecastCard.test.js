import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import ForecastCard from "./ForecastCard";
import { forecastData } from "../mock/mockData";
describe("ForecastCard", () => {
  test("renders without crash", () => {
    const component = render(
      <ForecastCard forecastData={forecastData.forecast.forecastday} />
    );
    const inputSearch = component.getByTestId("forecast");
    expect(inputSearch).toBeInTheDocument();
  });
  test("renders forecast for next days", () => {
    const component = render(
      <ForecastCard forecastData={forecastData.forecast.forecastday} />
    );
    const weatherCard = component.getAllByTestId("weatherCard");

    expect(weatherCard).toHaveLength(2);
  });
});
