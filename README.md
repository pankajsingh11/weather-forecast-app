# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## To clone this repo

Run command `git clone https://bitbucket.org/pankajsingh11/weather-forecast-app.git`

## About the app

**_ This is a simple weather app forecasting city weather data when user enters city name in the serach box and select a city from autocomplete search box. _**

### Points to note about the app

- By default app get's current user location and displays weather data of user current location
- There is a limitation with free tier of weather API that maximum three days weather forecast can be retrieved which includes the current day
- Due to above limitation, app displays current day weather data and forecast weather data for next two days
- Autocomplete search box has debounce of 200ms
- Used a third party library for autocomplete searchbox [autocomplete searchbox](https://github.com/sickdyd/react-search-autocomplete). Visit the link for complete list of props which can be configured
- used a third party library for howing toast notifactions of error

### Design ideas and limitations

- Since the app state is simple as of now, went ahead with using react hooks API to manage app state. If App has to grow and have a different future vision, would have went ahead using Redux or Mobx library to manage app state
- Haven't used any third party tool or library for logging and monitoring. If this was a scalabale app which had to be deployed to production, I would have implemented monitoring and alerting so that app availability and reliability can ne > 99% in prod
- Haven't implemented UI snapshot tests as the design is developer led and may not be finalized yet. Ideally, once the design is finalized and signed off from stakeholders, developer will then try and implement UI snapshot tests so that any regression in UI can easily be picked up during further developement
- Haven't tested UI components in detail with their styling as styling was something developer just came up with based on his ideas

## Available Scripts

In the project directory, you can run:

### Important configuration needed for the APP to run

**_Before starting the app, you need to get an API key from [weather API](https://www.weatherapi.com/my/) and put the API key in .env file created at project root location, same place where package.json file is_**

Make this entry into .env file REACT\*APP_API_KEY=<YOUR_API_KEY>

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn test:e2e`

Runs the End to End Integration tests in command line

### `yarn cypress:open`

Opens cypress GUI which helps to run the tests in interactively in browser and user can easily see results

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
